package com.demoapp.home.ui

import android.os.Bundle
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.demoapp.R
import com.demoapp.home.model.ImageResponse
import com.demoapp.home.ui.adapter.ImageAdapter
import com.demoapp.home.viewModel.HomeViewModel
import com.demoapp.utilities.AlertHelper
import com.demoapp.utilities.EndlessRecyclerViewScrollListener
import com.demoapp.utilities.Status
import dagger.hilt.android.AndroidEntryPoint


/**
 *created by Abhijeet Sharma on 19-01-2023
 */
@AndroidEntryPoint
class HomeActivity : AppCompatActivity(), SwipeRefreshLayout.OnRefreshListener,
    ImageAdapter.OnClickListener {
    private lateinit var swipeRefreshLayout: SwipeRefreshLayout
    private lateinit var recyclerView: RecyclerView
    private var pageStart: Int = 1
    private val homeViewModel: HomeViewModel by viewModels()
    private lateinit var imageAdapter: ImageAdapter
    private var currentPage: Int = pageStart
    private lateinit var listItems: ArrayList<ImageResponse.ImageResponseItem>


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_home)

        initView()
        observerData()
    }


    private fun observerData() {

        homeViewModel.getListData.observe(this) {
            when (it.status) {
                Status.SUCCESS -> {
                    swipeRefreshLayout.isRefreshing = false
                    imageAdapter.updateData(it.data as List<ImageResponse.ImageResponseItem>)
                }
                Status.ERROR -> {
                    swipeRefreshLayout.isRefreshing = false
                }
                Status.LOADING -> {
                    swipeRefreshLayout.isRefreshing = true
                }
            }
        }

    }


    private fun initView() {
        swipeRefreshLayout = findViewById(R.id.swipeRefresh)
        recyclerView = findViewById(R.id.recyclerView)
        swipeRefreshLayout.setOnRefreshListener(this)

        val layoutManager = LinearLayoutManager(this)
        recyclerView.layoutManager = layoutManager
        listItems = ArrayList()

        recyclerView.addOnScrollListener(object : EndlessRecyclerViewScrollListener(layoutManager) {
            override fun onLoadMore(page: Int, totalItemsCount: Int, view: RecyclerView?) {
                currentPage = page + 1
                homeViewModel.fetchDetails(currentPage)

            }
        }

        )
        val dividerItemDecoration = DividerItemDecoration(this, layoutManager.orientation)
        recyclerView.addItemDecoration(dividerItemDecoration)
        imageAdapter = ImageAdapter(
            this, listItems, this
        )
        recyclerView.adapter = imageAdapter
        homeViewModel.fetchDetails(currentPage)

    }


    override fun onRefresh() {
        currentPage = pageStart
        listItems = ArrayList()
        imageAdapter.updateData(listItems)
        homeViewModel.fetchDetails(currentPage)
    }

    override fun onItemClick(item: ImageResponse.ImageResponseItem) {
        AlertHelper.showAlert(this@HomeActivity, item.url)
    }

}