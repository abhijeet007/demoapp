package com.demoapp.home.model


import com.google.gson.annotations.SerializedName

/**
 *created by Abhijeet Sharma on 19-01-2023
 */

class ImageResponse : ArrayList<ImageResponse.ImageResponseItem>() {
    data class ImageResponseItem(
        @SerializedName("author")
        val author: String, // Alejandro Escamilla
        @SerializedName("download_url")
        val downloadUrl: String, // https://picsum.photos/id/0/5000/3333
        @SerializedName("height")
        val height: Int, // 3333
        @SerializedName("id")
        val id: String, // 0
        @SerializedName("url")
        val url: String, // https://unsplash.com/photos/yC-Yzbqy7PY
        @SerializedName("width")
        val width: Int // 5000
    )
}