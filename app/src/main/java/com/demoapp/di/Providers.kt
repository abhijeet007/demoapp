package com.demoapp.di

import com.demoapp.api.ApiHelper
import com.demoapp.api.ApiHelperImpl
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

/**
 *created by Abhijeet Sharma on 19-01-2023
 */
@Module
@InstallIn(SingletonComponent::class)
class Providers {

    @Provides
    @Singleton
    fun provideApiHelper(
        apiHelper: ApiHelperImpl
    ): ApiHelper = apiHelper
}