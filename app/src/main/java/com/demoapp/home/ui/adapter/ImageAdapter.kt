package com.demoapp.home.ui.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.demoapp.R
import com.demoapp.home.model.ImageResponse
import kotlinx.android.synthetic.main.list_item.view.*

/**
 *created by Abhijeet Sharma on 19-01-2023
 */

class ImageAdapter(
    private val context: Context,
    private val list: ArrayList<ImageResponse.ImageResponseItem>,
    private val onClickListener: OnClickListener
) :
    RecyclerView.Adapter<ImageAdapter.ImageViewHolder>() {

    class ImageViewHolder(private val context: Context, itemView: View) :
        RecyclerView.ViewHolder(itemView) {

        fun bind(item: ImageResponse.ImageResponseItem) {

            Glide.with(context).load(item.downloadUrl)
                .placeholder(R.drawable.default_placeholder)
                .into(itemView.ivPoster)

            itemView.title.text = item.author
            itemView.description.text = item.url

        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ImageViewHolder {
        val view =
            LayoutInflater.from(parent.context).inflate(R.layout.list_item, parent, false)


        return ImageViewHolder(context, view)
    }

    override fun getItemCount(): Int = list.size


    override fun getItemViewType(position: Int): Int {
        return position
    }

    override fun onBindViewHolder(holder: ImageViewHolder, position: Int) {

        holder.bind(list[position])
        holder.itemView.setOnClickListener {
            onClickListener.onItemClick(list[position])
        }
    }

    fun updateData(newList: List<ImageResponse.ImageResponseItem>) {
        if (newList.isEmpty()) {
            list.clear()
        } else {
            list.addAll(newList)
        }
        notifyDataSetChanged()
    }

    interface OnClickListener {

        fun onItemClick(item: ImageResponse.ImageResponseItem)
    }
}