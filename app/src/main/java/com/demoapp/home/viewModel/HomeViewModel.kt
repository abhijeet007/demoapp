package com.demoapp.home.viewModel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.demoapp.home.model.ImageResponse
import com.demoapp.home.repository.HomeRepository
import com.demoapp.utilities.ApiResponse
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.launch
import javax.inject.Inject


/**
 *created by Abhijeet Sharma on 19-01-2023
 */
@HiltViewModel
class HomeViewModel @Inject constructor(private val homeRepository: HomeRepository) : ViewModel() {

    private val _getListData = MutableLiveData<ApiResponse<ImageResponse>>()

    val getListData: LiveData<ApiResponse<ImageResponse>>
        get() = _getListData


    private fun getImageList(pageNo: Int) = viewModelScope.launch {
        _getListData.postValue(ApiResponse.loading(null))
        homeRepository.getDetails(pageNo).catch {
            _getListData.postValue(ApiResponse.error(it.message.toString(), null))
        }.collectLatest {
            _getListData.postValue(ApiResponse.success(it.body()))
        }
    }

    fun fetchDetails(pageNo: Int) = getImageList(pageNo)
}