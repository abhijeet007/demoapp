package com.demoapp.home.repository

import com.demoapp.api.ApiHelper
import com.demoapp.home.model.ImageResponse
import kotlinx.coroutines.flow.Flow
import retrofit2.Response
import javax.inject.Inject


/**
 *created by Abhijeet Sharma on 19-01-2023
 */
class HomeRepository @Inject constructor(private val apiHelper: ApiHelper) {

    suspend fun getDetails(pageNo: Int) : Flow<Response<ImageResponse>> = apiHelper.fetchImageList(pageNo)
}