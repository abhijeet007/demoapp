package com.demoapp.utilities

import android.app.AlertDialog
import android.app.Dialog
import android.content.Context
import com.demoapp.R


/**
 *created by Abhijeet Sharma on 19-01-2023
 */

object AlertHelper {

    fun showAlert(
        context: Context,
        message: String,

        ) {
        val alertDialog = AlertDialog.Builder(context)
        var dialog: Dialog? = null
        alertDialog.setTitle(context.getString(R.string.description))
        alertDialog.setCancelable(false)
        alertDialog.setMessage(message)
        alertDialog.setPositiveButton(context.getString(R.string.ok)) { _, _ ->
            dialog?.dismiss()
        }

        dialog = alertDialog.create()
        dialog.setCancelable(true)
        dialog.show()

    }

}