package com.demoapp.api

import com.demoapp.home.model.ImageResponse
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Query

/**
 *created by Abhijeet Sharma on 19-01-2023
 */


interface ApiService {

    @GET("v2/list")
    suspend fun getImageAndDetails(
        @Query("page") pageNo: Int,
        @Query("limit") pageLimit: Int,
    ): Response<ImageResponse>
}