package com.demoapp.api

import com.demoapp.home.model.ImageResponse
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import retrofit2.Response
import javax.inject.Inject
import javax.inject.Singleton

/**
 *created by Abhijeet Sharma on 19-01-2023
 */
@Singleton
class ApiHelperImpl @Inject constructor(
    private val apiService: ApiService
) : ApiHelper {

    override suspend fun fetchImageList(pageNo: Int): Flow<Response<ImageResponse>> {
        return flow { emit(apiService.getImageAndDetails(pageNo, 20)) }
    }

}