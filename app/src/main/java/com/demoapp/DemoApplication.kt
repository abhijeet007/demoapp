package com.demoapp

import android.app.Application
import dagger.hilt.android.HiltAndroidApp


/**
 *created by Abhijeet Sharma on 19-01-2023
 */
@HiltAndroidApp
class DemoApplication : Application() {

}