package com.demoapp.api

import com.demoapp.home.model.ImageResponse
import kotlinx.coroutines.flow.Flow
import retrofit2.Response


/**
 *created by Abhijeet Sharma on 19-01-2023
 */
interface ApiHelper {

    suspend fun fetchImageList(pageNo: Int): Flow<Response<ImageResponse>>
}