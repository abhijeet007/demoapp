package com.demoapp.utilities

enum class Status {
    SUCCESS,
    ERROR,
    LOADING
}